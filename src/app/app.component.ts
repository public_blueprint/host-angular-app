import { Component } from '@angular/core';

@Component({
  selector: 'mf1-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mf1';
}
