import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestpswdComponent } from './restpswd.component';

describe('RestpswdComponent', () => {
  let component: RestpswdComponent;
  let fixture: ComponentFixture<RestpswdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestpswdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RestpswdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
